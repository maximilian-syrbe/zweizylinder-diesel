import Keyboard from "./keyboard/Keyboard";
import Gamepad from "./gamepad/Gamepad";

export default class InputManager {
    private keyboard: Keyboard;
    private primaryGamepad: Gamepad;
    private gamepads: Gamepad[] = [];

    loadGamepad(): void {
        window.addEventListener("gamepadconnected", (e) => {
            let gamepad = new Gamepad(e.gamepad.index);
            this.gamepads[e.gamepad.index] = gamepad;
            if (!this.primaryGamepad || !navigator.getGamepads()[this.primaryGamepad.index].connected) {
                this.primaryGamepad = gamepad;
            }
        });

        window.addEventListener("gamepaddisconnected", (e) => {
            delete this.gamepads[e.gamepad.index];
            if (this.primaryGamepad.index === e.gamepad.index) {
                this.primaryGamepad = null;
            }
        });
    }

    loadKeyboard() {
        this.keyboard = new Keyboard();
        this.keyboard.load();
    }

    getPrimaryGamepad() {
        return this.primaryGamepad;
    }

    getKeyboard() {
        return this.keyboard;
    }

    update() {
        for (const g of this.gamepads) {
            g?.pollGamepad();
        }
    }
}
