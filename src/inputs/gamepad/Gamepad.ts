import {GamepadButton} from "./GamepadButton";

export default class Gamepad {

    private downButtons = new Map<GamepadButton, boolean>();
    private pressedButtons = new Map<GamepadButton, boolean>();

    constructor(public readonly index: number) {
    }

    isButtonDown(btn: GamepadButton) {
        return this.downButtons.get(btn);
    }

    printPressedKeys() {
        let gamepad = navigator.getGamepads()[this.index];

        gamepad.buttons.forEach((btn, x) => {
            if (btn.pressed) {
                console.log(x);
            }
        });
    }

    pollGamepad() {
        this.downButtons.clear();
        let gamepad = navigator.getGamepads()[this.index];

        if (!gamepad) {
            throw new Error(`Controller disconnected at idx: ${this.index}`);
        }

        gamepad.buttons.forEach((btn, idx) => {
            if (!this.pressedButtons.get(idx)) {
                this.downButtons.set(idx, btn.pressed);
            }
            this.pressedButtons.set(idx, btn.pressed);
        });
    }
}
