import GameEvent from "./GameEvent";
import Subscription from "./Subscription";

export default class EventManager {
    private listeners = new Map<string, ((evt: GameEvent) => void)[]>();

    on(event: string, callback: (evt: GameEvent) => void): Subscription {
        let listeners = this.listeners.get(event) || [];
        listeners.push(callback)
        this.listeners.set(event, listeners);
        return new Subscription(event, callback, this);
    }

    fire(event: string, data?: any): void {
        let listeners = this.listeners.get(event);
        if (listeners?.length) {
            for (const callbackFn of listeners) {
                callbackFn({
                    name: event,
                    data: data,
                    source: null
                });
            }
        }
    }

    unsubscribe(eventName: string, callback: (evt: GameEvent) => void): boolean {
        let listeners = this.listeners.get(eventName);

        if (!listeners?.length) {
            return false;
        }

        let idx = listeners.indexOf(callback);
        if (idx != -1) {
            let spliced = listeners.splice(idx, 1);
            return !!spliced.length;
        }
        return false;
    }

}
