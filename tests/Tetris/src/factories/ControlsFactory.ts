import {GamepadButton} from "../../../../src/inputs/gamepad/GamepadButton";

export default class ControlsFactory {

    static createKeyboardControls() {
        return new Map([
            [TetrisControls.MoveLeft, 'ArrowLeft'],
            [TetrisControls.MoveRight, 'ArrowRight'],
            [TetrisControls.RotateLeft, 'ArrowDown'],
            [TetrisControls.RotateRight, 'ArrowUp'],
            [TetrisControls.HardDrop, ' '],
            [TetrisControls.SoftDrop, 's'],
        ])
    }

    static createGamepadControls() {
        return new Map([
            [TetrisControls.MoveLeft, GamepadButton.LeftButtonInLeftCluster],
            [TetrisControls.MoveRight, GamepadButton.RightButtonInLeftCluster],
            [TetrisControls.RotateLeft, GamepadButton.TopLeftFrontButton],
            [TetrisControls.RotateRight, GamepadButton.TopRightFrontButton],
            [TetrisControls.HardDrop, GamepadButton.BottomRightFrontButton],
            [TetrisControls.SoftDrop, GamepadButton.BottomButtonInLeftCluster],
        ])
    }
}

export enum TetrisControls {
    MoveLeft= 'MoveLeft',
    MoveRight = 'MoveRight',
    SoftDrop = 'SoftDrop',
    HardDrop = 'HardDrop',
    RotateLeft = 'RotateLeft',
    RotateRight = 'RotateRight'
}
