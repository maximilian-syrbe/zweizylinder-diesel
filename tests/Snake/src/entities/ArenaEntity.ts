import GameEntity from "../../../../src/entities/GameEntity";
import Renderer from "../../../../src/graphics/Renderer";
import SnakeGame from "../SnakeGame";

export default class ArenaEntity extends GameEntity {
    readonly name = 'Arena';

    init(): void {
    }

    update(elapsedTime: number): void {
    }

    draw(renderer: Renderer): void {
        for (let i = 0; i < SnakeGame.BlockCount; i++) {
            for (let j = 0; j < SnakeGame.BlockCount; j++) {
                renderer.drawPixel(
                    Math.floor(i * SnakeGame.BlockSize + SnakeGame.MarginLeft),
                    Math.floor(j * SnakeGame.BlockSize + SnakeGame.MarginTop),
                    '#1a1a1a', SnakeGame.BlockSize, SnakeGame.BlockSize)
            }
        }
    }
}
