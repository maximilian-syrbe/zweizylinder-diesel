import GameEntity from "../../../../src/entities/GameEntity";
import Renderer from "../../../../src/graphics/Renderer";

export default class FpsEntity extends GameEntity {
    readonly name = 'Fps';

    lastFps: number;
    fpsCount: number;
    clock: number;

    init(): void {
        this.lastFps = 0;
        this.fpsCount = 0;
        this.clock = 0;
    }

    update(elapsedTime: number): void {
        this.clock += elapsedTime;
        this.fpsCount++;
        if (this.clock >= 1000) {
            this.lastFps = this.fpsCount;
            this.clock = 0;
            this.fpsCount = 0;
        }
    }

    draw(renderer: Renderer): void {
        renderer.drawText(`FPS: ${this.lastFps}`, 20, 20, 'white', 'monospace', 20);
    }

}
